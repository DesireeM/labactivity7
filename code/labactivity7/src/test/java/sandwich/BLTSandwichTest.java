/**
 * BLTSandwichTest is a class used for testing all the methods in the BLTSandwich class.
 * @author Sungeun Kim
 * @studentId 2042714
 * @version 10/17/2023
 */

package sandwich;

import static org.junit.Assert.*;

import org.junit.Test;

public class BLTSandwichTest {
    BLTSandwich sandwich = new BLTSandwich();

    /**
     * This method tests the constructor in the BLTSandwich if fillings are initialized correctly.
     */
    @Test
    public void testConstructor() {
        assertEquals("Bacon, Lettuce, Tomato", sandwich.getFilling());
    }

    /**
     * This method tests the getFilling method in the BLTSandwich if the initialized fillings are same as expected value.
     */
    @Test
    public void testGetFilling() {
        assertEquals("Bacon, Lettuce, Tomato", sandwich.getFilling());
    }

    /**
     * This method tests the addFilling method in the BLTSandwich.
     */
    @Test
    public void testAddFilling() {
        sandwich.addFilling("Cheese");
        assertEquals("Bacon, Lettuce, Tomato, Cheese", sandwich.getFilling());
    }

    /**
     * This method tests the isVegetarian method in the BLTSandwich if this returns false.
     */
    @Test
    public void testIsVegetarian() {
        assertFalse(sandwich.isVegetarian());
    }
}
