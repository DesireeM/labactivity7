/**  BLTSandwich is a class used for implementing the interface ISandwich.
 * @author Desiree Mariano
 * @studentid 2236344
 * @version 10/17/2023
 */

package sandwich;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit test for VegetarianSandwichTest.
 */

public class VegetarianSandwichTest {
  VegetarianSandwich sandwich = new TofuSandwich();
  /**
  * This test should return a empty string as filling
  * since that is what we declared it as
  */
  @Test
  public void VegetarianSandwichTest() {
    VegetarianSandwich vs = new CaesarSandwich();
    assertEquals("", vs.getFilling());
    assertEquals("", sandwich.getFilling());
  }

  /**
  * This test tells us if our sandwich is vegetarian
  * @return true
  */
  @Test
  public void isVegetarianShouldReturnTrue()
  {
    VegetarianSandwich sandwich = new CaesarSandwich();
    VegetarianSandwich tofusandwich = new TofuSandwich();

    assertFalse(sandwich.isVegetarian());
    assertTrue(tofusandwich.isVegetarian());
  }

  /**
  * This test should test that getFilling method works by checking
  * if the filling is the same to what we set it as
  */
  @Test
  public void testGetFilling() 
  {
    VegetarianSandwich sandwich = new CaesarSandwich();
    String filling = sandwich.getFilling();
    assertEquals("", filling);
  }


  /**
  * This test should test that addFilling method works by checking
  * if the filling is the same to what we set it as.
  */
  @Test
  public void testAddFilling() 
  {
    VegetarianSandwich sandwich = new CaesarSandwich();
    sandwich.addFilling("cucumber, hummus, lettuce");
    String filling = sandwich.getFilling();
    assertEquals("cucumber, hummus, lettuce, ", filling);

    // Test adding non-vegetarian topping (should throw an exception)
    try {
      sandwich.addFilling("chicken");
      fail("Expected IllegalArgumentException");
    } catch (IllegalArgumentException e) {
      // Expected exception
    }
  }

  @Test
  public void testIsVegetarian() {
    VegetarianSandwich tofusandwich = new TofuSandwich();
    VegetarianSandwich sandwich = new CaesarSandwich();
    assertFalse(sandwich.isVegetarian());
    assertTrue(tofusandwich.isVegetarian());
  }

  @Test
  public void testIsVegan() {
    VegetarianSandwich sandwich = new CaesarSandwich();
    VegetarianSandwich tofusandwich = new TofuSandwich();
    // Test when the filling contains "cheese"
    tofusandwich.addFilling("cheese");
    assertFalse(tofusandwich.isVegan());
    sandwich.addFilling("cheese");
    assertFalse(sandwich.isVegan());

    // Test when the filling contains "egg"
    sandwich = new CaesarSandwich();
    tofusandwich = new TofuSandwich();
    sandwich.addFilling("egg");
    assertFalse(sandwich.isVegan());
    assertTrue(tofusandwich.isVegan());

    

    // Test when the filling contains neither "cheese" nor "egg"
    
    tofusandwich = new TofuSandwich();
    tofusandwich.addFilling("tomato");
    assertTrue(tofusandwich.isVegan());
  }
}

