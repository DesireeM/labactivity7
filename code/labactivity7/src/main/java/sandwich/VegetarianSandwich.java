/**
 * Skeleton is a class used for implamenting the interface ISandwich.
 * @author Sungeun Kim
 * @studentId 2042714
 * @version 10/17/2023
 */

package sandwich;

public abstract class VegetarianSandwich implements ISandwich {

    private String filling;

    /**
     * This is constructor for VegetarianSandwich class.
     */
    public VegetarianSandwich () {
        this.filling = "";
    }

    /**
     * @return string such as “ham, cheese” or “cucumber, hummus” 
     */
    public String getFilling(){
        return this.filling;
    }
  
    /**
     * @return void, this is designed to add a specific filling to the sandwich.
     */
    public void addFilling(String topping) {
        String[] ToppingsNotVegan = {"chicken", "beef", "fish", "meat", "pork"};

        //If the topping is not vegetarian (contains any of ToppingsNotVegan), throw an exception.
        for (String nonVegToppings : ToppingsNotVegan) {
            if (topping.contains(nonVegToppings)) {
                throw new IllegalArgumentException("Non-vegetarian topping not allowed.");
            }
        }
        //If the topping is vegetarian, add it to the filling.
        filling += topping + ", ";    
    }  

    /**
     * @return boolean based on whether the sandwich is vegetarian or not.
     */
    public final boolean isVegetarian() {
        return true;
    }

     /**
     * @return boolean based on whether the filling String contains “cheese” or “egg”.
     */
    public boolean isVegan() {
        // Check if the filling contains "cheese" or "egg"
        if (filling.contains("cheese") || filling.contains("egg")) {
            return false;
        } else {
            return true;
        }
    }

    //Abstract method
    public abstract String getProtein();

}