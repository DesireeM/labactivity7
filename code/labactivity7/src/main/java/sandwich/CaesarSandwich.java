/**  BLTSandwich is a class used for implementing the interface ISandwich.
 * @author Desiree Mariano
 * @studentid 2236344
 * @version 10/17/2023
 */

package sandwich;
public class CaesarSandwich extends VegetarianSandwich {
private String filling;
    public CaesarSandwich(){
        this.filling = "Caesar Dressing";
    }

    @Override
    public String getProtein(){
        return "Anchovies";
    }

    @Override
    public boolean isVegetarian() {
        return false;
    }

}
