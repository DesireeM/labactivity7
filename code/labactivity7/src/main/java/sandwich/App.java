package sandwich;

public class App 
{
    public static void main( String[] args )
    {
        //1. compilation error!!
        //VegetarianSandwich vegSandwich = new VegetarianSandwich();
        //4
        // ISandwich tofuSandwich = new TofuSandwich();

        // //5
        // if (tofuSandwich instanceof ISandwich) 
        // {
        //     System.out.println("TofuSandwich is an instance of ISandwich.");
        // } 
        // else 
        // {
        // System.out.println("TofuSandwich is not an instance of ISandwich.");
        // }

        // //6
        // if (tofuSandwich instanceof VegetarianSandwich) {
        //     System.out.println("TofuSandwich is an instance of VegetarianSandwich.");
        // } 
        // else 
        // {
        //     System.out.println("TofuSandwich is not an instance of VegetarianSandwich.");
        // }
        //7
        // tofuSandwich.addFilling("chicken");
        // System.out.println("Added 'chicken' to TofuSandwich.");
        //8
       // tofuSandwich.isVegan(); wont work!
       //9
        // VegetarianSandwich newSandwich = ((VegetarianSandwich)tofuSandwich);
        // //10
        // newSandwich.isVegan();
        //11
        VegetarianSandwich vegSandwich = new CaesarSandwich();
        System.out.println(vegSandwich.isVegetarian());
    }
}
