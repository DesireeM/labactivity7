/**  BLTSandwich is a class used for implementing the interface ISandwich.
 * @author Desiree Mariano
 * @studentid 2236344
 * @version 10/17/2023
 */
package sandwich;

public class BLTSandwich implements ISandwich {
    private String filling;
    /**
     * the constructor of the BLTSandwich class
     */
    public BLTSandwich(){
        filling = "Bacon, Lettuce, Tomato";
    }
    /**
     * @return a string such as “ham, cheese” or “cucumber, hummus”
     */
    public String getFilling(){
        return this.filling;
    }

    /**
     * @return  This is designed to add a specific filling to the sandwich
     */
    public void addFilling(String topping){
      filling += ", " + topping;
    }

    /**
     * @return false This method will return a boolean based on whether the sandwich is vegetarian
        or not
     */
    public boolean isVegetarian(){
        return false;
    }
}
