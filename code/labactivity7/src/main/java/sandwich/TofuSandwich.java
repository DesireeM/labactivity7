/**
 * Skeleton is a class used for implamenting the interface ISandwich.
 * @author Sungeun Kim
 * @studentId 2042714
 * @version 10/17/2023
 */

 
package sandwich;

public class TofuSandwich extends VegetarianSandwich {
    
    private String filling;

    //Constructor that initialize filling field to be "Tofu"
    public TofuSandwich() {
        this.filling = "Tofu";
    }
    
    @Override
    public String getProtein() {
        return "Tofu";
    }
}
